[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](./LICENSE)

[![DUB](https://img.shields.io/github/tag/franck-talbart/back_to_the_future_the_ride_vr.svg)](https://github.com/franck-talbart/back_to_the_future_the_ride_vr)
=======

# Back to the Future: The Ride VR (BTTFTRVR)
![BTTFTRVR](Misc/pics/Back_to_the_Future_The_Ride_logo.png)

The biggest part of the work is done now.
This new release now support 2DOF and 3DOF simulators!

## Introduction

Back to the Future: The Ride (BTTFTR) was a simulator ride at Universal Studios theme parks, located at Universal Studios Florida and Universal Studios Hollywood.
The simulator was based on the Back to the Future movies. Unfortunately, it was permanently closed in 2007.

This project aims to bring the ride back to life in the most realistic way possible. Let’s call it BTTFTRVR (Back to the Future: The Ride Virtual Reality).



## Demo

Click on the thumbnail to play it:

[![Alt text](https://img.youtube.com/vi/kINSU0sgc3c/0.jpg)](https://www.youtube.com/watch?v=kINSU0sgc3c)

Complete Walkthrough with preshow:

[![Alt text](https://img.youtube.com/vi/t2OzJFJKEDw/0.jpg)](https://www.youtube.com/watch?v=t2OzJFJKEDw)

## Support

Happy with the project? You can buy me a coffee!
https://ko-fi.com/franckarts
Thanks!

## How it works

The original footage has been restored and remastered and is available online. This project provides a virtual reality environment to experience it, where the movie theater is faithfully recreated, using Unity and an Oculus Rift or HTC Vive.
In addition, a homemade simulator, along with a fan, is used to add a touch of magic to the experience. This project is compatible with SimTools to support a 3DOF motion simulator and the fan.

## Compilation

Dependencies:

* git
* Blender (mandatory to open the project!)
* Unity (tested with 2023.2.13f1 release)
* [Optional] SimTools to add a motion simulator
* [Optional] Motion Cancellation if a motion simulator is used (https://github.com/openvrmc/OpenVR-MotionCompensation)

OpenVR is a requirement (do not use the Oculus API) in order to run the motion cancellation tool for your motion simulator.

First, download the missing video and audio files from http://franck.talbart.free.fr/bttftrvr/ and place them in the Assets\BigAssets folder. You can use the deploy.bat script to automate this process.

### [Optional] Motion simulator

#### Arduino

The firmware used for the Arduino board can be found in Misc/arduino.

#### Plugin 

The SimTools plugin must be built and imported into the SimTools Game Manager. To create the plugin, navigate to Tools/SimTools Plugin/BTTFTRVRSimTools*/SimToolsGamePlugin/GamePlugin.
Generate the solution, then create a zip file containing the DLL file and the MaxMin file from the bin directory.
This zip file can then be imported into SimTools.

#### Gamedash

The fan is controller by Gamedash. The first dash gives the speed of the vehicle. The associated formula is the following:
```
MATH * 2
ROUND 0
PAD 3 0
```

Interface output:
```
S<Dash1>C
```

#### Simtools Game Engine

```
Output - bit range: 10
Output - type: Binary
BitsPerSec: 9600
DataBits: 8
Parity: None
Stop bits: 1
```

The Interface output is:
```
X1<Axis1a>CX2<Axis2a>CX3<Axis3a>CX4<Axis4a>CX5<Axis5a>CX6<Axis6a>C
```

The Shutdown output is:
```
XE00C
```

10ms

### Build 

To build the project, open it with Unity 2023. The generated executable must be named BTTFTRVR.exe. This is important, as the process name is used by SimTools to recognize the game.
SimTools is not mandatory to enjoy this project; a simple VR headset is enough. However, the telemetry features (3DOF motion simulator + fan) add a lot of fun! :)

## Nota Bene

At the beginning of the ride, please ensure that the motion cancellation is still working.
For some reasons that need to be investigated, the cancellation may be disabled when starting the app.
Additionally, please disable the OpenVR Input Simulator if it is installed.

This ride currently supports 3 axes; please do not enable the others, as it would reduce the effects:

DOF1 => Roll
DOF2 => Pitch
DOF3 => Heave


## Licence

The BTTFTRVR is released under the GNU General Public License, version 3. Please refer to [LICENSE](./LICENSE), for details.

## Authors

The author list is written in the [AUTHORS](./AUTHORS) file.

## Contact

franck@talbart.fr
