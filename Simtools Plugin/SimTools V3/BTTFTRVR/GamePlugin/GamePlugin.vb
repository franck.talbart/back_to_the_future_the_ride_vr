﻿Option Strict On
Option Explicit On

Imports System.Net
Imports System.Math
Imports System.Threading
Imports Game_PluginAPI_v3
Imports System.Runtime.InteropServices
Imports System.Text
Public Class GamePlugin
    Implements IPlugin_Game_v3
#Region "    '/// BUILT IN METHODS FOR SIMTOOLS ///"
    '/////////////////////////////////////////////////////
    '/// DO NOT CHANGE - DO NOT CHANGE - DO NOT CHANGE ///
    '/////////////////////////////////////////////////////

    'Output structures
    Private _PluginSettings As New IPlugin_Game_v3.Struct_PluginSettings
    Private _Motion_Outputs As New IPlugin_Game_v3.Struct_MotionOutput
    Private _Dash_Outputs As New IPlugin_Game_v3.Struct_DashOutput
    Private _Vibe_Outputs As New IPlugin_Game_v3.Struct_VibeOutput
    Private _Status_Outputs As New IPlugin_Game_v3.Struct_StatusOutput
    Private Property _OnlineID As String Implements IPlugin_Game_v3.OnlineID  'set what id number

    'Load settings
    Public Sub New()
        _Dash_Outputs.Reset()
        _PluginSettings._PluginVersion = Reflection.Assembly.GetExecutingAssembly.GetName.Version.ToString
        PluginSettings()
    End Sub

    'Output - plugin settings
    Public Function Get_PluginSettings() As IPlugin_Game_v3.Struct_PluginSettings Implements IPlugin_Game_v3.Get_Plugin_Settings
        Return _PluginSettings
    End Function

    'Output - status outputs
    Public Function Get_StatusOutputs() As IPlugin_Game_v3.Struct_StatusOutput Implements IPlugin_Game_v3.Get_Status_Outputs
        Return _Status_Outputs
    End Function

    'Output - motion outputs
    Public Function Get_MotionOutputs() As IPlugin_Game_v3.Struct_MotionOutput Implements IPlugin_Game_v3.Get_Motion_Outputs
        Return _Motion_Outputs
    End Function

    'Output - dash outputs
    Public Function Get_DashOutputs() As IPlugin_Game_v3.Struct_DashOutput Implements IPlugin_Game_v3.Get_Dash_Outputs
        Return _Dash_Outputs
    End Function

    'Output - vibe outputs
    Public Function Get_VibeOutputs() As IPlugin_Game_v3.Struct_VibeOutput Implements IPlugin_Game_v3.Get_Vibe_Outputs
        Return _Vibe_Outputs
    End Function

    'Reset data
    Public Sub PluginReset() Implements IPlugin_Game_v3.PluginReset
        _Motion_Outputs = New IPlugin_Game_v3.Struct_MotionOutput
        _Dash_Outputs.Reset()
        _Vibe_Outputs = New IPlugin_Game_v3.Struct_VibeOutput
    End Sub

    'Launch Shortcut 
    Event LaunchShortcut(ShortcutType As IPlugin_Game_v3.Shortcut_Type, OnlineID As String, Path As String) Implements IPlugin_Game_v3.LaunchShortcut
    Private Sub Create_LaunchShortcut(ShortcutType As IPlugin_Game_v3.Shortcut_Type, Path As String) Implements IPlugin_Game_v3.Create_LaunchShortcut
        RaiseEvent LaunchShortcut(ShortcutType, _OnlineID, Path)
    End Sub

    'Error Logging
    Event ErrorLog(ErrorLevel As IPlugin_Game_v3.Error_Level, Message As String) Implements IPlugin_Game_v3.ErrorLog
    Private Sub Log(ErrorLevel As IPlugin_Game_v3.Error_Level, Message As String) Implements IPlugin_Game_v3.Log
        RaiseEvent ErrorLog(ErrorLevel, Message)
    End Sub

    'Vibe type enum
    Enum VibeType
        NotUsed
        Afterburners
        Chassis
        Collision_LR
        Collision_FB
        Flaps
        Gear_Shift
        Landing_Gear
        Road_Detail
        Rpm
        Turbo
        Turbulence
        Weapons_Bombs
        Weapons_Cannons
        Weapons_Flairs
        Weapons_Guns
        Weapons_Other
        'Chassis_FL
        'Chassis_FR
        'Chassis_RL
        'Chassis_RR
    End Enum

    'Platform type enum
    Private Enum PlatformType
        PC
        Console
    End Enum

    'Game origin enum
    Enum GameOrigin
        CD
        Steam
        Epic
        Origin
    End Enum

    'Game type enum
    Enum GameType
        Air
        Land
        Sea
    End Enum

    'Motion type enum
    Enum Motion_Type
        Degree
        Acceleration
    End Enum

    '/////////////////////////////////////////////////////
    '/// DO NOT CHANGE - DO NOT CHANGE - DO NOT CHANGE ///
    '/////////////////////////////////////////////////////
#End Region
    '/////////////////////////////////////////////////////////////////////////////////////////////////////////////
    '///        SimTools GamePlugin - Edit the Setting below to provide support for your favorite game!        ///
    '/////////////////////////////////////////////////////////////////////////////////////////////////////////////

    'Plugin Settings
    Private Sub PluginSettings()
        '//////////////////////////////////////////////// 
        '/// Per Game Settings - Change for Each Game ///
        '////////////////////////////////////////////////
        _PluginSettings._PluginAuthorsName = "Franck Talbart" 'The plugin authors names (comma delimited)
        _PluginSettings._GameName = "BTTFTRVR" 'GameName - the displayed name in simtools
        _PluginSettings._ProcessName = "BTTFTRVR" 'the Process Name without the (".exe")
        _PluginSettings._RequiresPatchingPath = False 'do we need a path for patching?
        _PluginSettings._PlatformType = PlatformType.PC 'set the platform type
        _PluginSettings._GameOrigin = GameOrigin.CD 'set the game origin
        _PluginSettings._GameType = GameType.Land 'set the game type
        '_PluginSettings._GameID = "310560" 'set the game ID

        '//////////////////////////////////////////////// 
        '///              Auto Profiling              ///
        '////////////////////////////////////////////////
        _PluginSettings._AutoProfiling_Enabled = False 'does the plugin support auto profiling?
        'if set to true, '_Status_Outputs._CurrentVehicle' must get set with the motion data below.

        '//////////////////////////////////////////////// 
        '///       Game Provided Crash Variable       ///
        '////////////////////////////////////////////////
        _PluginSettings._GameProvidedCrashed_Enabled = False
        'if set to true, '_Motion_Outputs._HasCrashed' must get set with the motion data below.

        '////////////////////////////////////////////////
        '///      Supported Output for this Game      ///
        '////////////////////////////////////////////////
        _PluginSettings._MotionSupported_Roll = True
        _PluginSettings._MotionSupported_Pitch = True
        _PluginSettings._MotionSupported_Yaw = False
        _PluginSettings._MotionSupported_Heave = True
        _PluginSettings._MotionSupported_Sway = False
        _PluginSettings._MotionSupported_Surge = False
        _PluginSettings._MotionSupported_Extra1 = "" '(Add a name to turn it on)
        _PluginSettings._MotionSupported_Extra2 = "" '("" = off)
        _PluginSettings._MotionSupported_Extra3 = ""
        _PluginSettings._MotionSupported_Extra4 = ""
        _PluginSettings._MotionSupported_Extra5 = ""
        _PluginSettings._MotionSupported_Extra6 = ""

        '////////////////////////////////////////////////
        '///        Output Type for this Game         ///
        '////////////////////////////////////////////////
        _PluginSettings._MotionSupported_Roll_Type = Motion_Type.Degree 'Set the motion type
        _PluginSettings._MotionSupported_Pitch_Type = Motion_Type.Degree
        _PluginSettings._MotionSupported_Yaw_Type = Motion_Type.Degree
        _PluginSettings._MotionSupported_Heave_Type = Motion_Type.Degree 'Motion_Type.Acceleration
        _PluginSettings._MotionSupported_Sway_Type = Motion_Type.Acceleration
        _PluginSettings._MotionSupported_Surge_Type = Motion_Type.Acceleration
        _PluginSettings._MotionSupported_Extra1_Type = Motion_Type.Acceleration
        _PluginSettings._MotionSupported_Extra2_Type = Motion_Type.Acceleration
        _PluginSettings._MotionSupported_Extra3_Type = Motion_Type.Acceleration
        _PluginSettings._MotionSupported_Extra4_Type = Motion_Type.Acceleration
        _PluginSettings._MotionSupported_Extra5_Type = Motion_Type.Acceleration
        _PluginSettings._MotionSupported_Extra6_Type = Motion_Type.Acceleration

        '////////////////////////////////////////////////
        '///              Vibe Supported              ///
        '////////////////////////////////////////////////
        _PluginSettings._VibeSupport_VibeEnabled = False
        _PluginSettings._VibeSupported_Vibe01 = VibeType.Rpm
        _PluginSettings._VibeSupported_Vibe02 = VibeType.Gear_Shift
        _PluginSettings._VibeSupported_Vibe03 = VibeType.Collision_LR
        _PluginSettings._VibeSupported_Vibe04 = VibeType.Collision_FB
        _PluginSettings._VibeSupported_Vibe05 = VibeType.Road_Detail
        _PluginSettings._VibeSupported_Vibe06 = VibeType.Chassis
        _PluginSettings._VibeSupported_Vibe07 = VibeType.NotUsed
        _PluginSettings._VibeSupported_Vibe08 = VibeType.NotUsed
        _PluginSettings._VibeSupported_Vibe09 = VibeType.NotUsed
        _PluginSettings._VibeSupported_Vibe10 = VibeType.NotUsed

        '////////////////////////////////////////////////
        '///              Dash Supported              ///
        '////////////////////////////////////////////////
        _PluginSettings._DashSupport_DashEnabled = True

        'Speed
        _PluginSettings._DashSupported_Speed_GroundSpeed = True
        _PluginSettings._DashSupported_Speed_AirSpeedIndicator_ASI = False
        _PluginSettings._DashSupported_Speed_TrueWind_WhenStationary = False ': The wind you feel When stationary Is the True wind.
        _PluginSettings._DashSupported_Speed_VerticalSpeedIndicator_VSI = False

        'Rpm
        _PluginSettings._DashSupported_Rpm = True
        _PluginSettings._DashSupported_RPM_Max = False
        _PluginSettings._DashSupported_Rpm_MaxPercent = False

        'Position
        _PluginSettings._DashSupported_Position_Gear = True
        _PluginSettings._DashSupported_Position_AltitudeIndicator_AI = False '[horizon indicator] artificial horizon
        _PluginSettings._DashSupported_Position_DistanceTraveled = False 'Knotmeter: Measures a boat's speed, and knotlog records distance traveled through the water.
        _PluginSettings._DashSupported_Position_Distance2Destination = False 'Knotmeter: Measures a boat's speed, and knotlog records distance traveled through the water.
        _PluginSettings._DashSupported_Position_DepthFinder = False 'Indicates how deep the water Is so you can avoid running aground

        'Lights
        _PluginSettings._DashSupported_Lights_HeadLights = False
        _PluginSettings._DashSupported_Lights_BreakLights = False
        _PluginSettings._DashSupported_Lights_HazardOrWarning = False
        _PluginSettings._DashSupported_Lights_ShiftIndicator_Up = False
        _PluginSettings._DashSupported_Lights_ShiftIndicator_Down = False
        _PluginSettings._DashSupported_Lights_LeftTurnBlinker = False
        _PluginSettings._DashSupported_Lights_RightTurnBlinker = False
        _PluginSettings._DashSupported_Lights_TaxiLights = False
        _PluginSettings._DashSupported_Lights_TurnOffLights = False
        _PluginSettings._DashSupported_Lights_LandingLights = False
        _PluginSettings._DashSupported_Lights_NavigationLights = False
        _PluginSettings._DashSupported_Lights_AuxileryLights = False

        'Temperature
        _PluginSettings._DashSupported_Temp_OilTemp = False
        _PluginSettings._DashSupported_Temp_CoolantTemp = False
        _PluginSettings._DashSupported_Temp_EngineTemp = False
        _PluginSettings._DashSupported_Temp_CarTrackTemp = False
        _PluginSettings._DashSupported_Temp_PlaneAirTemp = False
        _PluginSettings._DashSupported_Temp_BoatWaterTemp = False
        _PluginSettings._DashSupported_Temp_TrackExitTemp = False

        'Pressure
        _PluginSettings._DashSupported_Pressure_FuelPressure = False
        _PluginSettings._DashSupported_Pressure_OilPressure = False
        _PluginSettings._DashSupported_Pressure_CoolantPressure = False
        _PluginSettings._DashSupported_Pressure_TurboPressure = False

        'Capacity
        _PluginSettings._DashSupported_Capacity_FuelLevel = False
        _PluginSettings._DashSupported_Capacity_FuelCapacity = False
        _PluginSettings._DashSupported_Capacity_BatteryLevel = False
        _PluginSettings._DashSupported_Capacity_BatteryCapacity = False

        'ID
        _PluginSettings._DashSupported_ID_TeamID = False
        _PluginSettings._DashSupported_ID_TrackID = False
        _PluginSettings._DashSupported_ID_VehicleID = False
        _PluginSettings._DashSupported_ID_DriverID = False

        'Lap
        _PluginSettings._DashSupported_Lap_Number = False
        _PluginSettings._DashSupported_Lap_BestTime = False
        _PluginSettings._DashSupported_Lap_PreviousTime = False
        _PluginSettings._DashSupported_Lap_CurrentTime = False
        _PluginSettings._DashSupported_Lap_CurrentPosition = False
        _PluginSettings._DashSupported_Lap_VehiclesinRace = False

        'Orientation
        _PluginSettings._DashSupported_Orient_GPS = False
        _PluginSettings._DashSupported_Orient_Compass = False
        _PluginSettings._DashSupported_Orient_AngleOfAttack_AoA = False
        _PluginSettings._DashSupported_Orient_TurnCoordinator_TC = False 'turn balance indicator
        _PluginSettings._DashSupported_Orient_HeadingIndicator_HI_HSI = False '(HI)Or(HSI)
        _PluginSettings._DashSupported_Orient_WindAngleInstrument = False ': Displays wind angle

        'Alarms
        _PluginSettings._DashSupported_Alarm_EngineOverHeating = False
        _PluginSettings._DashSupported_Alarm_BrakesOverHeating = False
        _PluginSettings._DashSupported_Alarm_WeaponsOverHeating = False
        _PluginSettings._DashSupported_Alarm_StallWarning = False
        _PluginSettings._DashSupported_Alarm_MissleLaunchDetected = False
        _PluginSettings._DashSupported_Alarm_FighterAircraftHaveBoost = False
        _PluginSettings._DashSupported_Alarm_AnchorDragging = False
        _PluginSettings._DashSupported_Alarm_ShallowWater = False

        'Damage
        _PluginSettings._DashSupported_Damage_DamageDetected = False
        _PluginSettings._DashSupported_Damage_ChassisDamage = False
        _PluginSettings._DashSupported_Damage_SuspensionDamage = False
        _PluginSettings._DashSupported_Damage_TireDamage = False
        _PluginSettings._DashSupported_Damage_PartsDetached = False

        'Weapons
        _PluginSettings._DashSupported_Weapons_GunsAmmoCount1 = False
        _PluginSettings._DashSupported_Weapons_GunsAmmoCount2 = False
        _PluginSettings._DashSupported_Weapons_BombsAmmoCount1 = False
        _PluginSettings._DashSupported_Weapons_BombsAmmoCount2 = False
        _PluginSettings._DashSupported_Weapons_CannonsAmmoCount1 = False
        _PluginSettings._DashSupported_Weapons_CannonsAmmoCount2 = False
        _PluginSettings._DashSupported_Weapons_FlairsAmmoCount1 = False
        _PluginSettings._DashSupported_Weapons_FlairsAmmoCount2 = False
        _PluginSettings._DashSupported_Weapons_OtherAmmoCount1 = False
        _PluginSettings._DashSupported_Weapons_OtherAmmoCount2 = False

        'Status
        _PluginSettings._DashSupported_Status_ABS = False
        _PluginSettings._DashSupported_Status_HandBreak = False
        _PluginSettings._DashSupported_Status_InPit = False
        _PluginSettings._DashSupported_Status_PitLimiter = False
        _PluginSettings._DashSupported_Status_AutoPilot = False
        _PluginSettings._DashSupported_Status_LandingGear = False
        _PluginSettings._DashSupported_Status_AirBrakes = False
        _PluginSettings._DashSupported_Status_Flaps = False
        _PluginSettings._DashSupported_Status_ChuteDeployed = False
        _PluginSettings._DashSupported_Status_LandingGearBayDoors = False
        _PluginSettings._DashSupported_Status_BombBayDoors = False
        _PluginSettings._DashSupported_Status_Canopy = False
        _PluginSettings._DashSupported_Status_Trim = False
        _PluginSettings._DashSupported_Status_AnchorUpDown = False
        _PluginSettings._DashSupported_Status_Flags = False
        _PluginSettings._DashSupported_Status_FrontArb = False
        _PluginSettings._DashSupported_Status_RearArb = False
        _PluginSettings._DashSupported_Status_TractionControl = False
        _PluginSettings._DashSupported_Status_Diff = False
        _PluginSettings._DashSupported_Status_BrakeBias = False

        'Extras         
        _PluginSettings._DashSupported_Dash01 = "" '(Add a name to turn it on)
        _PluginSettings._DashSupported_Dash02 = "" '("" = off)
        _PluginSettings._DashSupported_Dash03 = ""
        _PluginSettings._DashSupported_Dash04 = ""
        _PluginSettings._DashSupported_Dash05 = ""
        _PluginSettings._DashSupported_Dash06 = ""
        _PluginSettings._DashSupported_Dash07 = ""
        _PluginSettings._DashSupported_Dash08 = ""
        _PluginSettings._DashSupported_Dash09 = ""
        _PluginSettings._DashSupported_Dash10 = ""
        _PluginSettings._DashSupported_Dash11 = ""
        _PluginSettings._DashSupported_Dash12 = ""
        _PluginSettings._DashSupported_Dash13 = ""
        _PluginSettings._DashSupported_Dash14 = ""
    End Sub

    'Used when the Game Starts
    Public Sub GameStart() Implements IPlugin_Game_v3.GameStart
        'Do something on game start
        StartUDP_GameListener()
        _Start_MonitoringGame()
    End Sub

    'Used when the Game Stops
    Public Sub GameEnd() Implements IPlugin_Game_v3.GameEnd
        'Do something when game ends
        StopUDP_GameListener()
    End Sub

    'Tell the user where to Patch the Game 
    Public Function PatchPathInfo() As String Implements IPlugin_Game_v3.PatchPathInfo
        'Patching Info - See if they are on a 64 bit or 32 bit machine.
    End Function

    'Used to Patch a Game.
    Public Function PatchGame(MyPath As String) As Boolean Implements IPlugin_Game_v3.PatchGame
        'Unpatch - Always try to unpatch first (Should this be built into simtools?)
        UnPatchGame(MyPath)

        'Backup - No VR
        If IO.File.Exists(MyPath & "hardwaresettings\hardware_settings_config.xml") Then 'Backup Configuration File
            If IO.File.Exists(MyPath & "hardwaresettings\cfg.bak") = False Then
                FileCopy(MyPath & "hardwaresettings\hardware_settings_config.xml", MyPath & "hardwaresettings\cfg.BAK")
            End If
        End If

        'Backup - VR
        If IO.File.Exists(MyPath & "hardwaresettings\hardware_settings_config_vr.xml") Then 'Backup Configuration File
            If IO.File.Exists(MyPath & "hardwaresettings\cfgvr.bak") = False Then
                FileCopy(MyPath & "hardwaresettings\hardware_settings_config_vr.xml", MyPath & "hardwaresettings\cfgvr.BAK")
            End If
        End If

        Try
            'Patch - No VR
            If IO.File.Exists(MyPath & "hardwaresettings\hardware_settings_config.xml") Then
                Try
                    Dim MyXML = XDocument.Load(MyPath & "hardwaresettings\hardware_settings_config.xml")
                    MyXML.<hardware_settings_config>.<motion_platform>.<dbox>.@enabled = "false"
                    MyXML.<hardware_settings_config>.<motion_platform>.<udp>.@enabled = "true"
                    MyXML.<hardware_settings_config>.<motion_platform>.<udp>.@ip = "127.0.0.1"
                    MyXML.<hardware_settings_config>.<motion_platform>.<udp>.@port = "4123"
                    MyXML.<hardware_settings_config>.<motion_platform>.<udp>.@delay = "1"
                    MyXML.<hardware_settings_config>.<motion_platform>.<udp>.@extradata = "3"
                    MyXML.<hardware_settings_config>.<motion_platform>.<custom_udp>.@enabled = "false"
                    MyXML.<hardware_settings_config>.<motion_platform>.<fanatec>.@enabled = "false"
                    MyXML.Save(MyPath & "hardwaresettings\hardware_settings_config.xml")
                Catch ex As Exception
                    Return False
                End Try
            End If

            'Patch - VR
            If IO.File.Exists(MyPath & "hardwaresettings\hardware_settings_config_vr.xml") Then
                Try
                    Dim MyXML = XDocument.Load(MyPath & "hardwaresettings\hardware_settings_config_vr.xml")
                    MyXML.<hardware_settings_config>.<motion_platform>.<dbox>.@enabled = "false"
                    MyXML.<hardware_settings_config>.<motion_platform>.<udp>.@enabled = "true"
                    MyXML.<hardware_settings_config>.<motion_platform>.<udp>.@ip = "127.0.0.1"
                    MyXML.<hardware_settings_config>.<motion_platform>.<udp>.@port = "4123"
                    MyXML.<hardware_settings_config>.<motion_platform>.<udp>.@delay = "1"
                    MyXML.<hardware_settings_config>.<motion_platform>.<udp>.@extradata = "3"
                    MyXML.<hardware_settings_config>.<motion_platform>.<custom_udp>.@enabled = "false"
                    MyXML.<hardware_settings_config>.<motion_platform>.<fanatec>.@enabled = "false"
                    MyXML.Save(MyPath & "hardwaresettings\hardware_settings_config_vr.xml")
                Catch ex As Exception
                    Return False
                End Try
            End If

            'Return
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    'Used to UnPatch a Game.
    Public Function UnPatchGame(MyPath As String) As Boolean Implements IPlugin_Game_v3.UnPatchGame
        'Un-Patch Game

        'No VR
        If IO.File.Exists(MyPath & "hardwaresettings\cfg.BAK") Then 'Restore backup File
            If IO.File.Exists(MyPath & "hardwaresettings\hardware_settings_config.xml") Then
                IO.File.Delete(MyPath & "hardwaresettings\hardware_settings_config.xml")
            End If
            Rename(MyPath & "hardwaresettings\cfg.BAK", MyPath & "hardwaresettings\hardware_settings_config.xml")
        End If

        'VR
        If IO.File.Exists(MyPath & "hardwaresettings\cfgvr.BAK") Then 'Restore backup File
            If IO.File.Exists(MyPath & "hardwaresettings\hardware_settings_config_vr.xml") Then
                IO.File.Delete(MyPath & "hardwaresettings\hardware_settings_config_vr.xml")
            End If
            Rename(MyPath & "hardwaresettings\cfgvr.BAK", MyPath & "hardwaresettings\hardware_settings_config_vr.xml")
        End If

        'let simtools know we unpatched
        Return True
    End Function

    'Used to Validate the Pathing Path.
    Public Function ValidatePatchPath(MyPath As String) As Boolean Implements IPlugin_Game_v3.ValidatePatchPath
        'Validate Path
        If IO.File.Exists(MyPath & "hardwaresettings\hardware_settings_config.xml") Or IO.File.Exists(MyPath & "hardwaresettings\hardware_settings_config_vr.xml") = True Then
            Return True
        Else
            Return False
        End If
    End Function

    '(Optional) Amything extra needed to get the plugin running. (like in game settings)
    Public Function ExtraSetupInfo() As String Implements IPlugin_Game_v3.ExtraSetupInfo
        'Tell the user anything else needed to get the game working properly
        Return ("")   'return an empty string to dissable
    End Function

    '//////////////////////////////////////////////////////////////////
    '///   Place The Extra Needed Code For This Plugin Below Here   ///
    '//////////////////////////////////////////////////////////////////

    'Game Vars
    Private TimeOld As Single
    Private HeaveAcc As Single
    Private DeltaTime As Single
    Private HeaveSpeed As Single
    Private HeaveSpeedOld As Single = 0

    'Game Output Structure
    Private CodemastersData As New CodemasterDirtExtradata_3
    <StructLayout(LayoutKind.Sequential)>
    Private Structure CodemasterDirtExtradata_3
        'Public Lap As Single    'The number of laps since the session began. Restarting a Session in the game will set this back to 0.
        Public Time As Single   'Time in seconds since this telemetry session began Restarting a Session in the game will set this back to 0.
        Public LapTime As Single    'Time in seconds since the lap began. F1 2012 has a capture/send rate of 60 telemetry packets per second. This is 0 for an 'out lap'
        Public LapDistance As Single    'The distance in meters since the lap began. This can be negative for 'out' laps. If you cross the start finish line and then reverse back over it the LapDistance will shoot back up buytbut the Lap will stay the same
        '                                position on track in metres from the start/finish line
        Public Distance As Single   'The laps and percent of laps covered since the session began. The value is negative when you first enter a practice session. After you cross the start finish line for the first time it becomes positive (0). A value of 1.5 would indicate you're halfway through your second lap. 1.995 would indicate you're almost finished your second lap.
        Public X As Single  'The X position of the car in the world
        Public Y As Single  'The Y position of the car in the world
        Public Z As Single  'The Z position of the car in the world
        Public Speed As Single  'The car speed at this point in time. To get km/hr, multiply this value by 3.6 (I‘m not sure why codemasters uses this scale).
        Public XV As Single 'Velocity x
        Public YV As Single 'Velocity y
        Public ZV As Single 'Velocity z
        Public RX As Single 'Right vector XR
        Public RY As Single 'Right vector YR
        Public RZ As Single 'Right vector ZR
        Public FX As Single 'Forward vector XD
        Public FY As Single 'Forward vector YD
        Public FZ As Single 'Forward vector ZD
        Public SuspensionPositionRearLeft As Single 'The position of the rear left suspension
        Public SuspensionPositionRearRight As Single    'The position of the rear right suspension
        Public SuspensionPositionFrontLeft As Single    'The position of the front left suspension
        Public SuspensionPositionFrontRight As Single   'The position of the front right suspension
        Public SuspensionVelocityRearLeft As Single 'The speed of travel of the rear left suspension
        Public SuspensionVelocityRearRight As Single    'The speed of travel of the rear right suspension
        Public SuspensionVelocityFrontLeft As Single    'The speed of travel of the front left suspension
        Public SuspensionVelocityFrontRight As Single   'The speed of travel of the front right suspension
        Public WheelSpeedRearLeft As Single 'The speed the rear left wheel is travelling. I assume this value with Speed subtracted gives wheel spin. Although it’s a good 6 km/hr faster than the front, if that’s the case!
        Public WheelSpeedRearRight As Single    'The speed the rear right wheel is travelling.
        Public WheelSpeedFrontLeft As Single    'The speed the front left wheel is travelling
        Public WheelSpeedFrontRight As Single   'The speed the front right wheel is travelling
        Public Throttle As Single   'The percent of throttle applied. 1 = full throttle and 0 = no throttle
        Public Steer As Single  'The amount of left/right (-/+) input into steering. 0 = straight ahead.
        Public Brake As Single  'The percent of brake applied. 1 = full brake and 0 = no brake
        Public Clutch As Single 'This value will always be 0
        Public Gear As Single   'The current gear
        Public GForceLatitudinal As Single  'The amount of gforces being generated due to turning
        Public GForceLongitudinal As Single 'The amount of gforces being generated due to acceleration or braking
        Public CurrentLap As Single    'The current Lap. 0 = first lap across the start/finish line.
        Public EngineRevs As Single 'The RPM of the engine. This value needs to be multiplied by 10 to get the real RPM, so it would seen.
        Public Value38 As Single
        Public RacePosition As Single
        Public Value40 As Single
        Public Value41 As Single
        Public Value42 As Single
        Public Value43 As Single
        Public Value44 As Single
        Public Fuel_lt As Single
        Public Fuel_max As Single
        Public Value47 As Single
        Public Value48 As Single
        Public Value49 As Single
        Public Value50 As Single
        Public Value51 As Single '(wheel temperature [°K]?)
        Public Value52 As Single '(wheel temperature [°K]?)
        Public Value53 As Single '(wheel temperature [°K]?)
        Public Value54 As Single '(wheel temperature [°K]?) 
        Public Value55 As Single
        Public Value56 As Single
        Public Value57 As Single
        Public Value58 As Single
        Public FinishedLaps As Single
        Public TotalLaps As Single
        Public TrackLength As Single
        Public TimeForRace As Single
        Public Value63 As Single
        Public Value64 As Single
        Public Value65 As Single
    End Structure

    'UDP Motion Input
#Region "UDP Motion Input"
    'Motion Vars
    Private Motion_ThreadReceive As Thread
    Private Motion_UDPStarted As Boolean = False
    Private Motion_UdpClient As Sockets.UdpClient
    Private Motion_RemoteIpEndPoint As New IPEndPoint(IPAddress.Any, 0)

    'Start the UDP Listener
    Private Sub StartUDP_GameListener()
        If Motion_UDPStarted = False Then
            Motion_UDPStarted = True
            Motion_UdpClient = New Sockets.UdpClient(4123)
            Motion_ThreadReceive = New Thread(AddressOf Motion_PacketRecieved) With {
                .IsBackground = True
            }
            Motion_ThreadReceive.Start()
        End If
    End Sub

    'Stop the UDP Listener
    Private Sub StopUDP_GameListener()
        'close all UDP activity
        If Motion_UDPStarted = True Then
            Motion_UDPStarted = False
            On Error Resume Next
            Motion_UdpClient.Close()
            Motion_ThreadReceive.Abort()
        End If
    End Sub

    'Receive Messages
    Private Sub Motion_PacketRecieved()
        'set game active
        Game_ActivePackets = True

        Try
            'get the recieved bytes
            Dim ByteArray() As Byte = Motion_UdpClient.Receive(Motion_RemoteIpEndPoint)
            Dim ReceivedData As String = Encoding.UTF8.GetString(ByteArray)
            Dim telemetry As String() = receivedData.Split(New Char() {","c})

            'Motion
            _Motion_Outputs._Roll = CDbl(Val(telemetry(0)))
            _Motion_Outputs._Pitch = CDbl(Val(telemetry(1)))
            _Motion_Outputs._Heave = CDbl(Val(telemetry(2)))
            _Motion_Outputs._Yaw = CDbl(Val(telemetry(3)))
            _Motion_Outputs._Sway = CDbl(Val(telemetry(4)))
            _Motion_Outputs._Surge = CDbl(Val(telemetry(5)))
            _Motion_Outputs._Extra1 = CDbl(Val(telemetry(6)))
            _Motion_Outputs._Extra2 = CDbl(Val(telemetry(7)))
            _Dash_Outputs._Speed_GroundSpeed = CDbl(Val(telemetry(8))).ToString
        Catch ex As Exception
        End Try
        'see if we need to fire again
        If Motion_UDPStarted = True Then
            Motion_ThreadReceive = New Thread(AddressOf Motion_PacketRecieved) With {
                 .IsBackground = True
            }
            Motion_ThreadReceive.Start()
        End If
    End Sub
#End Region

    'Game Monitoring
#Region "Game Monitoring"
    'start monitoring game - start looking for start and stops in the game
    Private Game_ActivePackets As Boolean = False
    Private Sub _Start_MonitoringGame()
        'start background thread
        Game_ActivePackets = False
        Dim td_MonitoringGame As New Thread(AddressOf Game_Monitor) With {
            .IsBackground = True
        }
        td_MonitoringGame.Start()
    End Sub

    'Used to reset the output when no data is changing
    Private Sub Game_Monitor()
        Dim Game_IsActive As Boolean = False
        While Motion_UDPStarted = True
            'see if we have had some changes
            If Game_ActivePackets = True Then
                If Game_IsActive = False Then
                    'set active state and OutputActive flag
                    Game_IsActive = True
                    _Status_Outputs._OutputActive = True
                End If

                'reset active packets flag
                Game_ActivePackets = False
            Else
                If Game_IsActive = True Then
                    'set active state and OutputActive flag
                    Game_IsActive = False
                    _Status_Outputs._OutputActive = False
                End If
            End If

            'pause (let the game run a bit)
            Thread.Sleep(250) '1/4 sec
        End While
    End Sub
#End Region
End Class
