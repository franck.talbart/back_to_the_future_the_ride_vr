/*
 * Original source code from sirnoname : x-sim.de
 Updated by Franck Talbart to add the wind simulator with L298N support
 Value in Game dash: 
 MATH * 5
 ROUND 0
 PAD 3 0
changelog:
  1.6
  - changing var for array of (int) values
  
  1.5 (to do in this sketch) :
  - add a switch to reset position of motors
  1.4 :
  - you can use a potentiometer to modify the global power of the motion simulator
  - to disable this functionnality you need to set GP_select = 0;

  1.23 : 
  - When serial send XE00C, the motion plateform will be placed in original position
  - Use of packet serial communication instead of simplified serial (more stable)
  

  10 bits (16 bits value / 64)
  57600 bauds 8N1
  
  Simtools : X1<Axis1a>CX2<Axis2a>CX3<Axis3a>CX4<Axis4a>CX5<Axis5a>CX6<Axis6a>C

  XE00C - End         
  Pin out of arduino for Sabertooth

  Pin 2 - TX data to connect to S1 Sabertooth 1
  Pin 3 - TX data to connect to S1 Sabertooth 2
  Pin 4 - TX data to connect to S1 Sabertooth 3

  Analog Pins

  Pin A0 - input of feedback positioning from motor 1
  Pin A1 - input of feedback positioning from motor 2
  Pin A2 - input of feedback positioning from motor 3
  Pin A3 - input of feedback positioning from motor 4
  Pin A4 - input of feedback positioning from motor 5
  Pin A5 - input of feedback positioning from motor 6
  
  Pin A11 - input of potentiometer to define the global power

  Pin 11 - switch to move the motion simulator in standby value. connect to GND

  As well 5v and GND pins tapped in to feed feedback pots too.
*/

#include <Sabertooth.h>
#include <SoftwareSerial.h>

//Some speed test switches for testers ;)
#define FASTADC  1 //Hack to speed up the arduino analogue read function, comment out with // to disable this hack

// defines for setting and clearing register bits
#ifndef cbi
  #define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
  #define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

#define LOWBYTE(v)   ((unsigned char) (v))        //Read
#define HIGHBYTE(v)  ((unsigned char) (((unsigned int) (v)) >> 8))
#define BYTELOW(v)   (*(((unsigned char *) (&v) + 1)))      //Write
#define BYTEHIGH(v)  (*((unsigned char *) (&v)))

int DeadZone = 9; //increase this value to reduce vibrations of motors
int ReadAnalog = 3;

int EmergencyPin = 8;   // Emergency switch (GND)

int ENA=5; // Wind / L298N Pin 5 (PWM)
int IN1=6; // Wind / L298N Pin 6 (IN1)
int IN2=7; // Wind / L298N Pin 7 (IN2)

//motors array : Pin, currentAnalog, target, Minpot, MaxPot, pot deadzone, standby value, P, I, D, integrated error, last error, Power
// Original: maj car partait en dehors des limites des potards sur BTTF
//int MinPot = 62;
int MinPot = 72;

// Original: maj car partait en dehors des limites des potards sur BTTF
//int MaxPot = 962;
int MaxPot = 952;

int motors[6][13] = { {A0, 0, 0, MinPot, MaxPot, 0, 512, 100, 0, 100, 0, 0, 0},  //motor 1
                      {A1, 0, 0, MinPot, MaxPot, 0, 512, 100, 0, 100, 0, 0, 0},  //motor 2
                      {A2, 0, 0, MinPot, MaxPot, 0, 512, 100, 0, 100, 0, 0, 0},  //motor 3
                      {A3, 0, 0, MinPot, MaxPot, 0, 512, 100, 0, 100, 0, 0, 0},  //motor 4
                      {A4, 0, 0, MinPot, MaxPot, 0, 512, 100, 0, 100, 0, 0, 0},  //motor 5
                      {A5, 0, 0, MinPot, MaxPot, 0, 512, 100, 0, 100, 0, 0, 0}}; //motor 6
                      
int GlobalPowerPin    = 12;  //use a potentiometer to define the power of the motion simulator on pin A11 (12)
int GP_select         = 0; //1:use potentiometer 0:always 100%
double GlobalPower    = 1.00; //100%

int buffer=0;
int buffercount=-1;
int oldSpeed;
int commandbuffer[6]={0};

int disable  = 1; //Motor stop flag

SoftwareSerial SWSerial1(NOT_A_PIN, 2); //Pin 2 utilisé pour communiquer avec la Sabertooth
SoftwareSerial SWSerial2(NOT_A_PIN, 3); //Pin 3 utilisé pour communiquer avec la Sabertooth
SoftwareSerial SWSerial3(NOT_A_PIN, 4); //Pin 3 utilisé pour communiquer avec la Sabertooth
Sabertooth ST1(128, SWSerial1); //nom de l'objet communication serie avec une pin differente de TX
Sabertooth ST2(128, SWSerial2); //nom de l'objet communication serie avec une pin differente de TX
Sabertooth ST3(128, SWSerial3); //nom de l'objet communication serie avec une pin differente de TX

void setup()
{
  Serial.begin(57600);

  SWSerial1.begin(115200); //boutons 1 et 2 OFF
  SWSerial2.begin(115200); //boutons 1 et 2 OFF
  SWSerial3.begin(115200); //boutons 1 et 2 OFF
    
  ST_stop();
  
  disable=1;

  pinMode(EmergencyPin, INPUT_PULLUP);

  // L298N
  pinMode(ENA,OUTPUT);
  pinMode(IN1,OUTPUT);
  pinMode(IN2,OUTPUT);
  analogWrite(ENA,0); // Disable
  
  // Direction  
  digitalWrite(IN1,HIGH); 
  digitalWrite(IN2,LOW);
  #if FASTADC
    // set analogue prescale to 16
    sbi(ADCSRA,ADPS2) ;
    cbi(ADCSRA,ADPS1) ;
    cbi(ADCSRA,ADPS0) ;
  #endif
}

void ST_stop()
{
    ST1.motor(1, 0);
    ST1.motor(2, 0);
    ST2.motor(1, 0);
    ST2.motor(2, 0);
    ST3.motor(1, 0);
    ST3.motor(2, 0);
    
    ST1.stop();
    ST2.stop();
    ST3.stop();
}

void FeedbackPotWorker()
{
  for(int z=0;z<6;z++)
  {
    motors[z][1]=0; //set to 0 all current analog
  }
   
  for(int i=0;i<ReadAnalog;i++)
  {
    for(int z=0;z<6;z++)
    {
      motors[z][1] += analogRead(motors[z][0]);
    }
  }

  for(int z=0;z<6;z++)
  {
    motors[z][1] /= ReadAnalog;
  }  
}

void SerialWorker()
{
  while(Serial.available()) 
  {
    buffer = Serial.read();
    if(buffercount == -1)
    {
      if(buffer == 'X' || buffer == 'S')
      {
        buffercount=1;
        commandbuffer[0] = buffer;
      }
    }
    else
    {
      commandbuffer[buffercount++]=buffer;
      if(buffercount > 4)
      {
        if(commandbuffer[4] == 'C')
        {
          if(commandbuffer[0] == 'X')
          {   
            ParseCommand();
          }
          else if(commandbuffer[0] == 'S')
          {  
            int SpeedGameDash = ((commandbuffer[1]-48)*100) + ((commandbuffer[2]-48)*10) + // Take values 1-3.
                                ((commandbuffer[3]-48)*1);
            SpeedGameDash = SpeedGameDash>255?255:SpeedGameDash;             
            if (oldSpeed != SpeedGameDash)
            {
              analogWrite(ENA,SpeedGameDash);
              oldSpeed = SpeedGameDash;
            }
          }
        }
        buffercount=-1;
      }
    }
  }
}

void ParseCommand()
{
  int diff;
  int i;
  bool commande = false;

  switch (commandbuffer[1])
  {
    case '1':
      i = 0;
      commande = true;
      break;
      
    case '2':
      i = 1;
      commande = true;
      break;
      
    case '3':
      i = 2;
      commande = true;
      break;
      
    case '4':
      i = 3;
      commande = true;
      break;
      
    case '5':
      i = 4;
      commande = true;
      break;
      
    case '6':
      i = 5;
      commande = true;
      break;
      
    case 'E':
      commande = false;
      unsigned long start;
      unsigned long time;
      start=millis();
  
      for(int z=0;z<6;z++)
      {
        motors[z][2]=motors[z][6]; //reset to standby position
      }
    
      time = millis();
    
      while(time < (start + 1500)) //1.5s
      {
        FeedbackPotWorker();
        CalculatePID();
        SetPWM();
        time=millis();
      }

      ST_stop();

      disable=1;
      break;

    default:
      commande = false;
      return;
      break;
  }

  if (commande)
  {
    commande = false;
    motors[i][2]=(commandbuffer[2]*256)+commandbuffer[3]; //target
    diff = constrain((GlobalPower * (motors[i][2] - 512)), -511, 511) + 512;
    motors[i][2]=map(diff,0,1023,motors[i][3],motors[i][4]); //ajustement en fonction des min et max
    disable=0;
  }
}

int updateMotorPid(int targetPosition, int currentPosition, int numMot)   
{
  float error = (float)targetPosition - (float)currentPosition; 
  motors[numMot][10] += error;
  motors[numMot][11] = error;
  float pTerm_motor = ((float)motors[numMot][7] * error) / 100.0;
  float iTerm_motor = ((float)motors[numMot][8] * constrain(motors[numMot][10], -100, 100)) / 100.0;
  float dTerm_motor = ((float)motors[numMot][9] * (error - motors[numMot][11])) / 100.0;

  return constrain((pTerm_motor + iTerm_motor + dTerm_motor)/2, -127, 127);
}

void CalculatePID()
{
  for (int z=0;z<6;z++)
  {
    motors[z][12] = updateMotorPid(motors[z][2],motors[z][1], z);
  }
}

void SetPWM()
{
  for(int z=0;z<6;z++)
  {
    if (abs(motors[z][2] - motors[z][1]) <= (DeadZone + motors[z][5]))
    {
      motors[z][12] = 0;
    }
  }
  
  ST1.motor(1, motors[0][12]);
  ST1.motor(2, motors[1][12]);
  ST2.motor(1, motors[2][12]);
  ST2.motor(2, motors[3][12]);
  ST3.motor(1, motors[4][12]);
  ST3.motor(2, motors[5][12]);  
}

void loop()
{
  //Program loop
  while (1==1) //Important hack: Use this own real time loop code without arduino framework delays
  {
    FeedbackPotWorker();
    if (GP_select == 1)
    {
      GlobalPower = double(analogRead(GlobalPowerPin)) / 512.0 ;
    }
    else GlobalPower = 1.00;
    
    if (digitalRead(EmergencyPin) == LOW)
    {
      for (int z=0;z<6;z++)
      {
        motors[z][2] = motors[z][6]; //target = stdby
      }
    }
    
    else  SerialWorker();
    
    CalculatePID();
    if(disable==0 || digitalRead(EmergencyPin) == LOW)
    {
      SetPWM();
    }
  }
}
