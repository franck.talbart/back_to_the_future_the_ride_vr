using System.Collections;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.XR;


public class PreshowRoom : MonoBehaviour
{
    private GameObject[] lights;
    private bool bypass_preshow;

    // Use this for initialization
    public void Start()
    {
        // /!\ !!! Use this to bypass the preshow !!! /!\
        bypass_preshow = false;
        lights = GameObject.FindGameObjectsWithTag("light_preshow");
        StartCoroutine(run());
    }

    IEnumerator run()
    {
        VideoPlayer videoPlayer = GameObject.Find("Screen Preshow").GetComponent<VideoPlayer>();
        if (!bypass_preshow)
        {           
            while ((videoPlayer.frame / videoPlayer.frameRate) < (3 * 60 + 59.5f))
            {
                yield return new WaitForSeconds(1);
            }
            
            for (int i = 0; i < lights.Length; ++i)
            {
                while (lights[i].GetComponent<Light>().intensity > 0)
                {
                    lights[i].GetComponent<Light>().intensity -= 0.1f;
                    yield return new WaitForSeconds(0.1f);
                }
            }
        }

        
        videoPlayer.Stop();
        (transform.parent.gameObject.GetComponent("Main") as MonoBehaviour).enabled = true;

        GameObject xrrig = GameObject.Find("XRRig Preshow");
        xrrig.SetActive(false);
        AudioListener listener = xrrig.GetComponentInChildren<AudioListener>();
        listener.enabled = false;
    }
}
 
