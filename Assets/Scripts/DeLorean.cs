/* This script handle the motion simulator
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Video;

public class DeLorean : MonoBehaviour
{
    private Dictionary<string, string> config = new Dictionary<string, string>();
    // The 2 telemetry files were produced with Video Ride Creator. The first one is used for the 3DOF motion simulator
    // It can be read by the video ride player
    // The 2nd one is used for the fan (speed)
    private int nb_lines = 0, time_start_car;
    private bool run_thread = true;
    private int cpt_line = -1, old_cpt_line = -1;
    private System.Threading.Thread telemetry_thread;
    private string[] metrics;
    // public because the reference is given from the editor
    public GameObject de_lorean;
    // public because the reference is given from the editor
    public VideoPlayer imax;
    public TextAsset config_file, telemetry_main_file, telemetry_dashboard_file;
    IPEndPoint remote_end_point;
    static UdpClient client;

    public void Start()
    {
        string[] lines = config_file.text.Split('\n');
        foreach (string line in lines)
        {
            string[] items = line.Split('=');
            if (items.Length == 2)
            {
                config[items[0].Trim()] = items[1].Trim();
            }
        }

        remote_end_point = new IPEndPoint(IPAddress.Parse(config["ip"]), Int32.Parse(config["port"]));
        client = new UdpClient();
        string[] lines_main = telemetry_main_file.text.Split("\n");
        string[] lines_dash = telemetry_dashboard_file.text.Split("\n");
        metrics = new string[lines_main.Length];
        string sep = "\t";
        // The biggest part of the work must be done here as we want to have the best performances
        // in the code which is performing the motions
        CultureInfo culture = CultureInfo.CreateSpecificCulture("fr-FR");
        while (nb_lines < lines_main.Length)
        {
            string[] words_main = lines_main[nb_lines].Split(sep.ToCharArray());
            string[] words_dash = lines_dash[nb_lines].Split(sep.ToCharArray());

            double speed = double.Parse(words_dash[0], NumberStyles.Float, culture);
            if (speed < 0) speed = 0;
            speed = (speed / float.Parse(config["val_max"])) * 141f; // 141 kmh == 88 miles per hour (max speed) :)
            // Send Telemetry.
            try
            {
                metrics[nb_lines] = String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}",
                                                   (int)double.Parse(words_main[0], NumberStyles.Float, culture),
                                                   (int)double.Parse(words_main[1], NumberStyles.Float, culture),
                                                   (int)double.Parse(words_main[2], NumberStyles.Float, culture),
                                                   (int)double.Parse(words_main[3], NumberStyles.Float, culture),
                                                   (int)double.Parse(words_main[4], NumberStyles.Float, culture),
                                                   (int)double.Parse(words_main[5], NumberStyles.Float, culture),
                                                   0,
                                                   0,
                                                   (int)speed);
            }
            catch(FormatException e)
            {
                Debug.LogError("FormatException caught: " + e.Message);
                Debug.LogError(lines_main[nb_lines]);
            }
            nb_lines++;
        }
        time_start_car = 5 * Convert.ToInt16(config["nb_points_per_sec"]); // s
       
        telemetry_thread = new System.Threading.Thread(Send_telemetry);
        telemetry_thread.IsBackground = true;
        telemetry_thread.Start();
        cpt_line = 0;
    }

    // Update is called once per frame
    public void Update()
    {
            cpt_line = (int)((imax.frame / imax.frameRate) * Convert.ToInt16(config["nb_points_per_sec"]));

            // This part must be as simple as possible and should not take more than 40 ms to be executed
            // Otherwhise telemetry data may be ignored!
            if (cpt_line >= 0)
            {
                if (cpt_line < metrics.Length)
                {
                    // Moving up the car
                    if (de_lorean.transform.transform.position.y < 4 && cpt_line > time_start_car)
                        de_lorean.transform.transform.position += de_lorean.transform.transform.up * Time.deltaTime;
                    cpt_line++;
                }
                else
                {
                    // Moving down the car
                    if (de_lorean.transform.transform.position.y > 0.546)
                        de_lorean.transform.transform.position -= de_lorean.transform.transform.up * Time.deltaTime;
                }
            }
    }

    private void OnDestroy()
    {
        run_thread = false;
    }

    private void Send_telemetry()
    {
        // This part should also be as fast as possible to not loose telemetry data
        while (run_thread)
        {
            if (cpt_line != old_cpt_line && cpt_line < metrics.Length)
            {
                old_cpt_line = cpt_line;

                byte[] data = Encoding.Default.GetBytes(metrics[cpt_line]);
                client.Send(data, data.Length, remote_end_point);
            }
        }
    }
}