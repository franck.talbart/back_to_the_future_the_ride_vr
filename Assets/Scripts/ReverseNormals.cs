﻿/*
This script is used to reverse the multiple faces of a 3D model.
Otherwhise, if the viewer looks at the other side, the faces will be transparent.

This code is used to render the black sphere. This sphere is designed to block the incoming lights. The black faces are reversed inside the sphere.
Doing so makes the sphere opaque from inside. It has been designed using Unity.

The screen was designed with Blender, with the faces on the expected side. It is NOT a sphere and this script is NOT used for it.

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class ReverseNormals : MonoBehaviour
{

    void Start()
    {
        MeshFilter filter = GetComponent(typeof(MeshFilter)) as MeshFilter;
        if (filter != null)
        {
            Mesh mesh = filter.mesh;

            Vector3[] normals = mesh.normals;
            for (int i = 0; i < normals.Length; i++)
                normals[i] = -normals[i];
            mesh.normals = normals;

            for (int m = 0; m < mesh.subMeshCount; m++)
            {
                int[] triangles = mesh.GetTriangles(m);
                for (int i = 0; i < triangles.Length; i += 3)
                {
                    int temp = triangles[i + 0];
                    triangles[i + 0] = triangles[i + 1];
                    triangles[i + 1] = temp;
                }
                mesh.SetTriangles(triangles, m);
            }
        }
    }
}