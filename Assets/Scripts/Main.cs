﻿using System.Collections;
using UnityEngine;
using UnityEngine.Video;
public class Main : MonoBehaviour
{
    public VideoPlayer player_main, player_screen, player_speed, player_time1, player_time2, player_time3;
    public AudioSource aud_src_main, aud_src_screen;
    public RenderControl garage_rc;
    public ParticleSystem smoke;
    private GameObject de_lorean;
    public Light light_room, light_blinking;

    // Use this for initialization
    public void Start()
    {
        GameObject xrrig = GameObject.Find("XRRig Main");
        AudioListener listener = xrrig.GetComponentInChildren<AudioListener>();
        listener.enabled = true;
        xrrig.SetActive(true);

        de_lorean = GameObject.Find("DeLorean");
        light_room = GameObject.Find("Directional Light").GetComponent<Light>();
        light_blinking = GameObject.Find("Blinking Light").GetComponent<Light>();
        StartCoroutine(run());
    }

    public void OnApplicationQuit()
    {
         Debug.Log("Application ending after " + Time.time + " seconds");
    }

    IEnumerator run()
    {
        // Full scenario
        MeshRenderer r_main = this.player_main.GetComponent<MeshRenderer>();
        MeshRenderer r_screen = this.player_screen.GetComponent<MeshRenderer>();
        r_main.enabled = false;
        r_screen.enabled = false;
        light_room.enabled = true;
        r_screen.enabled = true;
        this.player_screen.Play();
        this.player_speed.Play();
        this.player_time1.Play();
        this.player_time2.Play();
        this.player_time3.Play();

        while (light_room.GetComponent<Light>().intensity < 1.5)
        {
            light_room.GetComponent<Light>().intensity += 0.1f;
            yield return new WaitForSeconds(0.1f);
        }

        while ((this.player_screen.frame / player_screen.frameRate) < 80.5f)
        {
            yield return new WaitForSeconds(0.1f);
        }

        light_room.enabled = false;
        yield return new WaitForSeconds(1.8f);
        // Start moving the DeLorean
        (de_lorean.GetComponent("DeLorean") as MonoBehaviour).enabled = true; 
        yield return new WaitForSeconds(0.2f);    
        this.player_main.Play();
        this.aud_src_main.Play();
        smoke.Play();
        StartCoroutine(Blink_Light());
        yield return new WaitForSeconds(2);
        smoke.Stop();
        yield return new WaitForSeconds(8.2f);
        this.garage_rc.Disable();
        yield return new WaitForSeconds(0.5f);
        r_main.enabled = true;

        while ((this.player_main.frame / player_main.frameRate) < 238.5f)
        {
            yield return new WaitForSeconds(0.1f);
        }

        this.garage_rc.Enable();
		smoke.Play();
		yield return new WaitForSeconds(9);
        r_main.enabled = false;
        light_room.enabled = true;
        smoke.Stop();
    }

    IEnumerator Blink_Light()
    {
        yield return new WaitForSeconds(7);
        for (int i = 0; i < 40; ++i)
        {
            light_blinking.enabled = !light_blinking.enabled;
            yield return new WaitForSeconds(0.1f);
        }
    }
}
 
